Yayi
====

Image processing and mathematical morphology framework in C++ and Python.

Yayi is a open-source image processing framework with a focus on Mathematical Morphology. It is specifically designed
for writing new image processing algorithms in C++, and for rapid prototyping in Python.

The core of Yayi is entirely written in C++, mainly using template code which achieves a high level of
genericity. It implements some of the main concepts of Mathematical Morphology into an efficient and proven design.
Yayi aims at providing robust, efficient and flexible algorithms for image analysis, but also reference algorithms
for Mathematical Morphology.

The python interface provides a simple way for using the main notions and functions of mathematical morphology.
The export uses the boost.python framework.

The whole project uses `cmake`_ for building. Some bindings with `setup.py` are being developed in order to be able
to just `pip install yayi`.

It is released under the very permissive Boost license.

.. _boost: http://www.boost.org

.. _cmake: http://www.cmake.org


Key features
------------

* Image I/O: png (8/16/32), tiff (8/16/32), jpeg, hfd5
* Image arithmetic, pixel-wise comparisons, logical combinations
* Color space transformation (xyY, XYZ, LAB, YUV, etc)
* structuring element creation and manipulation
* Basic mathematical morphology: erosion, dilation, opening, closing (grey scale), hit-or-miss, white/black hat
* connected components labeling (background, adjacency graph...), measures on connected components
* morphological reconstruction and levelings
* segmentation: watershed transform



Installation
------------
Yayi needs `boost`_, `cmake`_ and a decent C++11 compiler to build. You should install those tools in your system, preferably
using your package manager prior to building Yayi.

.. warning:: as soon as you build and install Yayi, Yayi becomes dependent on the `boost` libraries installed on your system.
   If you upgrade `boost` to a new version, then it will likely break Yayi. As a matter of fact, python package management
   does not communicate with the /native/ package management of your operating system.


.. note:: some of the dependencies are shipped with Yayi directly, but on some platforms (Linux) the dependencies provided
          by the package manager are preferred.

After the installation, the following code should work:

.. code:: python

   import yayi
   yayi.IO.readPNG('my/png/file.png")

.. note:: currently not compiling the Python extensions with the HDF5 support.

Linux Ubuntu
~~~~~~~~~~~~
Ubuntu is shipped with its own package manager, and it is straightforward to install packages. An Ubuntu package can be found
from the following PPA `launchpadppa`_ on Launchpad. The instructions to add this PPA to your system and install Yayi are also
provided there.

.. _launchpadppa: https://launchpad.net/~raffi-enficiaud-x/+archive/ubuntu/yayi

Despite the existing binaries from the PPA, if you want to compile Yayi locally, you have to first install its dependencies.
For that, enter the following command in a Terminal:

.. code:: shell

  sudo apt-get install boost, cmake, python-numpy, python-pip, libjpeg8-dev, zlib1g-dev, libpng12-dev, libtiff4-dev, libhdf5-dev

and then just type (and wait)

.. code:: shell

  pip install yayi


OSX
~~~
On OSX, a wheel package is provided and it contains all the dependencies (especially boost). If you are lucky
the wheel is for your specific version of OSX/Python. If you are less lucky you should build Yayi from the sources.
For that, first install the dependencies with eg.`brew`_

.. code:: shell

  brew install boost
  brew install cmake

and then just type the following (and wait):

.. code:: shell

  # numpy should be installed on a default Mac
  pip install numpy
  pip install yayi


Win32
~~~~~

On Windows, binary packages (installer and wheel package) are provided, but for Python 2.7 64-bits only. If this is not your version of Python/Win architecture,
then you need to build Yayi on your platform like the other platforms. However, you are kind of lucky because you are not obliged to use a specific
toolchain for that (so you can build yayi with Visual 2013 community and you are not obliged to have Visual 2008 or some obsolete toolchain).

If you need to build Yayi, first install `cmake`_ and a binary version of `boost`_ (or build it yourself). Then you have two options:

* build yayi with `setup.py`
* build yayi from `pip`

With `setup.py`, extract the source and then type (and wait)

.. code:: shell

  python setup.py build_cmake --boostroot=/my_boost/installation/location/boost_1_59/ install

With `pip`, this is almost the same, you have to pass the previous option like this:

.. code:: shell

  pip install --upgrade --install-option "build_cmake" --install-option "--boostroot=/my_boost/installation/location/boost_1_59/" yayi-XXXX.tar.gz
