#!python
# -*- coding: UTF-8 -*-


import os

from cmake_pip.cmake_extension import ExtensionCMake, setup, build_ext
from distutils.util import convert_path

# extension created by cmake
ext_yayi = ExtensionCMake('yayi',
                          cmake_external_project='//Users/raffi/Personnel/YayiBitbucket',
                          cmake_locate_extensions=True)


def _get_version():
    """"Convenient function to get the version of this package"""

    ns = {}
    version_path = convert_path('yayi/version.py')
    if not os.path.exists(version_path):
        return None
    with open(version_path) as version_file:
        exec(version_file.read(), ns)

    return ns['__version__']


classifiers = [
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'Intended Audience :: Information Technology',
    'Intended Audience :: Science/Research',
    'Operating System :: MacOS :: MacOS X',
    'Operating System :: Microsoft :: Windows :: Windows NT/2000',
    'Operating System :: POSIX :: Linux',
    'Programming Language :: C++',
    'Programming Language :: Python :: 2.7',
    'Topic :: Scientific/Engineering :: Image Recognition',
    'Topic :: Scientific/Engineering :: Mathematics',
]

keywords = 'image processing', 'mathematical morphology', \
           'multidimensional images', 'multispectral images', \
           'image segmentation', 'erosion', 'dilation', \
           'opening', 'closing', 'hit-or-miss', 'connected components'

setup(
    name='yayi',
    version=_get_version(),

    author='Raffi Enficiaud',
    author_email='raffi.enficiaud@free.fr',

    # cmake extension, mainly to declare this package as non-pure and to
    # intercept the cmake related options
    # if the name is a subpackage (yayi.sthg), it is not listed in the top_level.txt file
    ext_modules=[ext_yayi],  #[ExtensionCMake('yayi.bin', [])],

    url='http://raffi.enficiaud.free.fr',
    packages=['yayi', 'yayi.extensions', 'yayi.tests'],
    package_data={'yayi': ['bin/*.*']},
    #extensions=ext_yayi,
    classifiers=classifiers,
    keywords=keywords,
    license='Boost Software License - Version 1.0 - August 17th, 2003',
    description='Yayi toolbox for image processing and mathematical morphology',
    long_description=open('README.txt').read(),
    #  zip_safe = False # automatic with the extensions
)
